<?php
$isPost = false;
$geoData = array(
    'Birmingham' => array(52.4666670, -1.8833330),
    'Bradford' => array(53.8000000, -1.7521000),
    'Bristol' => array(51.4500000, -2.5833000),
    'Edinburgh' => array(55.9531000, -3.1889000),
    'Glasgow' => array(55.8580000, -4.2590000),
    'Leeds' => array(53.7997000, -1.5492000),
    'Liverpool' => array(53.4000000, -3.0000000),
    'London' => array(51.5072000, -0.1275000),
    'Manchester' => array(53.4667000, -2.3333000),
    'Sheffield' => array(53.3836000, -1.4669000)
);
if (count($_POST)) {
    require_once __DIR__ . '/../vendor/autoload.php';
    $geo = new \Geodist\Geodist($geoData);
    $postCity = $_POST['city'];
    if (in_array($postCity, array_keys($geoData))) {
        $isPost = true;
        $cityCoordinate = $geo->getCityCoordinates($postCity);
        $near = \Geodist\Geodist::sortListByDistance($geo->findNearCity($postCity), $cityCoordinate);
    }
}
?>
<html>
<head>

</head>
<body>
<form method="post">
    Find places 100 miles from
    <select name="city">
        <option>Select one</option>
        <?php foreach ($geoData as $key => $value) { ?>
            <?php $selected = (isset($postCity) && ($key == $postCity)) ? ' selected="selected"' : '';?>
            <option value="<?= htmlspecialchars($key) ?>"<?= $selected?>><?= htmlspecialchars($key) ?></option>
        <?php } ?>
    </select>
    <input type="submit">
</form>
<?php if ($isPost) { ?>
    <?php if (count($near)) { ?>
        <p>Found:</p>
        <ul>
            <?php foreach ($near as $city => $coordinate) { ?>
                <li><?= $city ?></li>
            <?php } ?>
        </ul>
    <?php } ?>
<?php } ?>
</body>
</html>
