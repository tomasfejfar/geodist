<?php
use Geodist\Geodist;

class BaseTest extends \PHPUnit_Framework_TestCase
{
    protected $_geo;

    public function testCreateWithData()
    {
        $data = $this->getValidGeoData();
        $geo = new Geodist($data);
        $this->assertEquals(count($data), count($geo->getData()));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCreateWithInvalidDataFails()
    {
        $data = $this->getInvalidGeoData();
        $geo = new Geodist($data);
    }

    public function testCanQueryByCity()
    {
        $city = 'Bristol';
        $geo = new Geodist($this->getValidGeoData());
        $near = $geo->findNearCity($city);
        $this->assertEquals(array('Birmingham'), array_keys($near));
        $this->assertArrayNotHasKey($city, $near);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Unknown city "Prague"
     */
    public function testUnknownCityWillFail()
    {
        $city = 'Prague';
        $geo = new Geodist($this->getValidGeoData());
        $geo->findNearCity($city);
    }

    public function testCanQueryByCoordinate()
    {
        $coordinate = new Location\Coordinate(51.4500000, -2.5833000);
        $geo = new Geodist($this->getValidGeoData());
        $near = $geo->findNearCoordinate($coordinate);
        $this->assertEquals(array('Birmingham', 'Bristol'), array_keys($near));
    }

    public function testSortByDistance()
    {
        $geo = new Geodist($this->getValidGeoData());
        $list = $geo->getData();
        $coordinate = new Location\Coordinate(51.5072000, -0.1275000);
        $sortedList = Geodist::sortListByDistance($list, $coordinate);
        $expected = array(
            'London',
            'Birmingham',
            'Bristol',
            'Sheffield',
            'Manchester',
            'Leeds',
            'Bradford',
            'Liverpool',
            'Edinburgh',
            'Glasgow',
        );
        $this->assertEquals($expected, array_keys($sortedList));
    }

    public function testCanGetCityCoordinates()
    {
        $geo = new Geodist($this->getValidGeoData());
        $coordinate = $geo->getCityCoordinates('London');
        $expectedLat = 51.5072000;
        $expectedLng = -0.1275000;
        $this->assertEquals($expectedLat, $coordinate->getLat());
        $this->assertEquals($expectedLng, $coordinate->getLng());
    }

    protected function getValidGeoData()
    {
        return array(
            'Birmingham' => array(52.4666670, -1.8833330),
            'Bradford' => array(53.8000000, -1.7521000),
            'Bristol' => array(51.4500000, -2.5833000),
            'Edinburgh' => array(55.9531000, -3.1889000),
            'Glasgow' => array(55.8580000, -4.2590000),
            'Leeds' => array(53.7997000, -1.5492000),
            'Liverpool' => array(53.4000000, -3.0000000),
            'London' => array(51.5072000, -0.1275000),
            'Manchester' => array(53.4667000, -2.3333000),
            'Sheffield' => array(53.3836000, -1.4669000)
        );
    }

    protected function getInvalidGeoData()
    {
        return array(
            'Birmingham' => array('a', 'x'),
            'Bradford' => array(53.8000000, -1.7521000),
            'Bristol' => array(51.4500000, -2.5833000),
            'Edinburgh' => array(55.9531000, -3.1889000),
            'Glasgow' => array(55.8580000, -4.2590000),
            'Leeds' => array(53.7997000, -1.5492000),
            'Liverpool' => array(53.4000000, -3.0000000),
            'London' => array(51.5072000, -0.1275000),
            'Manchester' => array(53.4667000, -2.3333000),
            'Sheffield' => array(53.3836000, -1.4669000)
        );
    }
}
