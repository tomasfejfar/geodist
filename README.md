Docs
====

* `public/index.php` - simple webapp to test the classes
* `src/Geodist/*` - classes
* `tests/Geodist/*` - test files

Run `phpunit` in `/` to run tests. Run `php -S localhost:80` in `public` to start server (PHP5.4+). I committed all the
composer files to the repo for your convenience (in case you don't have composer installed).
