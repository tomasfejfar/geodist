<?php
namespace Geodist;

use Location;

class Geodist
{
    protected $_data;

    /**
     * @param array $data associative array $city => array ($lat, $lng)
     */
    public function __construct($data)
    {
        $this->setData($data);
    }

    /**
     * @param array $list of coordinates
     * @param Location\Coordinate $referencePoint
     * @return mixed
     */
    public static function sortListByDistance($list, Location\Coordinate $referencePoint)
    {
        $calculator = new Location\Distance\Vincenty();
        uasort($list, function ($a, $b) use ($referencePoint, $calculator) {
            return self::distanceComparator($a, $b, $referencePoint, $calculator);
        });
        return $list;
    }

    /**
     * @param Location\Coordinate $a
     * @param Location\Coordinate $b
     * @param Location\Coordinate $referencePoint
     * @param Location\Distance\DistanceInterface $calculator
     * @return int
     */
    public static function distanceComparator(Location\Coordinate $a, Location\Coordinate $b, Location\Coordinate $referencePoint, Location\Distance\DistanceInterface $calculator)
    {
        $distanceA = $a->getDistance($referencePoint, $calculator);
        $distanceB = $b->getDistance($referencePoint, $calculator);
        if ($distanceA == $distanceB) {
            return 0;
        }
        return ($distanceA < $distanceB) ? -1 : 1;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param $data
     * @return Geodist
     */
    public function setData($data)
    {
        $this->_data = array();
        foreach ($data as $key => $item) {
            $this->_data[$key] = new Location\Coordinate($item[0], $item[1]);
        }
        return $this;
    }

    /**
     * @param string $city
     * @return Location\Coordinate
     */
    public function getCityCoordinates($city)
    {
        if (!array_key_exists($city, $this->_data)) {
            throw new \InvalidArgumentException(sprintf('Unknown city "%s"', $city));
        }
        return $this->_data[$city];
    }

    /**
     * @param string $city
     * @return array
     * @throws \InvalidArgumentException
     */
    public function findNearCity($city)
    {
        $referencePoint = $this->getCityCoordinates($city);
        $nearCoordinate = $this->findNearCoordinate($referencePoint);
        unset($nearCoordinate[$city]);
        return $nearCoordinate;
    }

    /**
     * @param Location\Coordinate $coordinate
     * @return array
     */
    public function findNearCoordinate(Location\Coordinate $coordinate)
    {
        $distance = 160934.4; // 100 miles in meters;
        $calculator = new Location\Distance\Vincenty();
        $result = array_filter($this->_data, function ($city) use ($coordinate, $calculator, $distance) {
            return $coordinate->getDistance($city, $calculator) < $distance;
        });
        return $result;
    }
}
